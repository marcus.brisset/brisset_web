document.addEventListener("keyup", () => {
	
	let firstNameInput = document.getElementById("first_name");
	let lastNameInput = document.getElementById("last_name");
	
	let firstNameNeedle = escape(firstNameInput.value.toLowerCase());
	let lastNameNeedle = escape(lastNameInput.value.toLowerCase());
	let userList = document.getElementsByClassName("user");
	let hitCounter = 0;
	let hitDisplay = document.getElementById("search_hits");
	
	if (firstNameNeedle.match(/\*/g) || lastNameNeedle.match(/\*/g)) {
		firstNameInput.value = firstNameNeedle = firstNameNeedle.replace(/\*/g, "");
		lastNameInput.value = lastNameNeedle = lastNameNeedle.replace(/\*/g, "");
	}
	
	for (i = 0; i < userList.length; i++) {
		let firstNameHaystack = userList[i].children[1].innerHTML.toLowerCase();
		let lastNameHaystack = userList[i].children[2].innerHTML.toLowerCase();
		
		if(firstNameHaystack.search(firstNameNeedle) === -1 || lastNameHaystack.search(lastNameNeedle) === -1) {
			userList[i].classList.add("_hidden");
		} else {
			userList[i].classList.remove("_hidden");
			hitCounter++;
		}
	}
	
	if (firstNameNeedle !== "" || lastNameNeedle !== "") {
		hitDisplay.innerHTML = hitCounter + " hit" + (hitCounter !== 1 ? "s" : "") + ".";
	} else {
		hitDisplay.innerHTML = "";
	}
	
});

function toggleNav() {
	
	let navBarTrigger = document.getElementById("nav-trigger");
	let navBar = document.getElementById("nav-main");

	navBar.classList.toggle("_hidden");
	navBarTrigger.classList.toggle("open");
	
}