document.addEventListener("click", (e) => {
	
//	COUNTER

	if (e.target.id === "counterIncrement") {
		let counterOutput = document.getElementById("counterOutput");

		let counter = parseInt(counterOutput.value);
		counter = (isNaN(counter) || counter < 0) ? 0 : counter + 1;
		counterOutput.value = counter;
	}


//	CALC

	if (e.target.parentElement.id === "calcButtons") {
		let calcNumber1 = document.getElementById("calcNumber1");
		let calcNumber2 = document.getElementById("calcNumber2");
		let calcResultOutput = document.getElementById("calcResultOutput");

		//making sure we get numbers, if not returns NaN
		let number1 = parseFloat(calcNumber1.value.replaceAll(/(\,)+/g, "."));
		let number2 = parseFloat(calcNumber2.value.replaceAll(/(\,)+/g, "."));
		
		//update input fields in case of invalid characters
		calcNumber1.value = number1;
		calcNumber2.value = number2;
		
		// if NaN change values to 0 
		if(isNaN(number1)) {
			number1 = 0;
			calcNumber1.value = 0;
		}
		if(isNaN(number2)) {
			number2 = 0;
			calcNumber2.value = 0;
		}

		switch (e.target.id) {
			case "calcAdd":
				calcResult = number1 +  number2;
				break;
			case "calcSub":
				calcResult = number1 - number2;
				break;
			case "calcMul":
				calcResult = number1 * number2;
				break;
			case "calcDiv":
				if (number2 === 0) {
					alert("Division by 0! Divisor changed to 1");
					calcNumber2.value = 1;
					calcResult = 0;
				} else {
					calcResult = number1 / number2;
				}	
		}

		calcResultOutput.value = calcResult;
	}
	
	
//	STRING CONCAT

	if (e.target.parentElement.id === "concatButtons" || e.target.id === "clearStringInput") {
		let stringInput = document.getElementById("stringInput");
		let concatOutput = document.getElementById("concatOutput");

		switch (e.target.id) {
			case "clearStringInput":
				stringInput.value = "";
				break;

			case "clearConcatOutput":
				concatOutput.innerHTML = "";
				break;

			case "concatStrings":
				concatOutput.innerHTML += stringInput.value.replaceAll(/(<|>|&)+/g, "");
				break;
			case "subString":
				let strLen = concatOutput.innerHTML.length;
				if (strLen !== 0) {
					concatOutput.innerHTML = concatOutput.innerHTML.substr(0, strLen - 1);
				} else {
					alert ("Text is empty!");
				}
		}
	}
	
	
//	COLOR MIXER
	
	if (e.target.id === "mixColors") {
		let hasRed = document.getElementById("red").checked;
		let hasGreen = document.getElementById("green").checked;
		let hasBlue = document.getElementById("blue").checked;
		let mixedColor = document.getElementById("mixedColor");
		let resultColor = "#";
		
		resultColor += hasRed ? "FF" : "00";
		resultColor += hasGreen ? "FF" : "00";
		resultColor += hasBlue ? "FF" : "00";
		
		mixedColor.style.backgroundColor = resultColor;
	}
	
});