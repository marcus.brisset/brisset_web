<?php
require("src/constants.php");
require("src/mysql_init.php");
require("src/smarty/libs/Smarty.class.php");
require("src/modules/Nav.php");

$smarty = new Smarty();
$smarty->setTemplateDir("src/templates");

if (isset($_COOKIE["mode"])) {
	$color_mode = $_COOKIE["mode"];
	$color_mode_toggler = ($color_mode == "dark") ? "light" : "dark";
} else {
	$color_mode = "dark";
	$color_mode_toggler = "light";
}

$smarty->assign("color_mode", ["current" => $color_mode, "toggler" => $color_mode_toggler, "toggler_uc" => ucfirst($color_mode_toggler)]);

$current_url = $_SERVER["PHP_SELF"];
$current_url_full = $current_url . "?" . $_SERVER["QUERY_STRING"];

$nav = new Nav();
$nav->setActiveUrl($current_url);

$nav->addItem("index.php", "home");
$nav->addItem("about.php", "about");
$nav->addItem("javascript.php", "javascript");
$nav->addItem("contact.php", "contact");
$nav->addItem("disclaimer.php", "disclaimer");

$smarty->assign("nav_items", $nav->getItems());