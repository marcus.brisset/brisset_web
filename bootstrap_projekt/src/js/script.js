/*
 *	Color Mode Toggle
 *	(removed the onload listener for sprit.PHP.js)
 */

window.addEventListener("load", () => {
	let cookieArray = document.cookie.split(";");
	let savedMode = "";
	
	for (cookiePair of cookieArray) {
		let cookieSplit = cookiePair.split("=");
		if (cookieSplit[0] === "mode") {
			savedMode = cookieSplit[1];
		}
	}
	
	if (savedMode !== "") {
		toggleColorMode(savedMode);
	}
});

document.getElementById("colorModeToggler").addEventListener("click", () => {
	let colorMode = document.getElementById("colorMode");
	let colorModeToggler = document.getElementById("colorModeToggler");
	
	if (colorMode.href.includes("dark")) {
		toggleColorMode("light", true);
		// remove follower
		if (document.getElementById("follower")) {
			document.getElementById("follower").classList.add("global-hidden");
		}
	} else {
		toggleColorMode("dark", true);
		// add follower
		if (document.getElementById("follower")) {
			document.getElementById("follower").classList.remove("global-hidden");
		}
	}
});

function toggleColorMode(mode, setCookie = false) {
	let colorMode = document.getElementById("colorMode");
	let colorModeToggler = document.getElementById("colorModeToggler");
	
	switch (mode) {
		case "light":
		//if dark switch to light
		colorMode.setAttribute("href", "src/css/modes/light.css?v1.0");
		colorModeToggler.src = "src/img/icons/dark_mode.png";
		colorModeToggler.alt = "Dark Mode";
		colorModeToggler.title = "Switch to Dark Mode";
		if (setCookie)
			document.cookie = "mode=light";
		break;
	case "dark":
		//if light switch to dark
		colorMode.setAttribute("href", "src/css/modes/dark.css?v1.0");
		colorModeToggler.src = "src/img/icons/light_mode.png";
		colorModeToggler.alt = "Light Mode";
		colorModeToggler.title = "Switch to Light Mode";
		if (setCookie)
			document.cookie = "mode=dark";
		break;
	}
}


/*
 *	Turns the bg dark when reaching the end of the page
 */

window.addEventListener("scroll", () => {
	let scroll = window.scrollY;
	let scrollMax = document.documentElement.scrollHeight - document.documentElement.clientHeight;
	
	//let testLabel = document.getElementById("test-label").offsetTop;
	
	let backToTop = document.getElementById("backToTop");
	if (scroll > 0) {
		backToTop.classList.remove("back-to-top-wrap--hidden");
	} else {
		backToTop.classList.add("back-to-top-wrap--hidden");
	}
	
	if (scroll > scrollMax - 100 && scroll !== 0) {
		document.body.classList.add("bg--end-of-page");
	} else {
		document.body.classList.remove("bg--end-of-page");
	}
});

/*
 *	# Navbar search function
 *	
 *	Firstly, strips all "haystack" classified elements of its HTML-tags to get
 *	a clean string without breaking some element's inner-tag stuff.
 *	(Since I couldn't figure out how to properly exclude them by using
 *	additional RegEx, this will have to do for the time being.)
 *	Then puts "<span>" tags around the matching strings. Restores initial
 *	content (thus adding back priorly removed HMTL-tags) when search bar is
 *	left empty.
 */

var haystackBackup = [];
var firstSearchInput = true;

document.getElementById("navSearchBar").addEventListener("keyup", () => {
	
		sleep().then( () => {
			
			let needle = document.getElementById("navSearchBar").value;
			let haystack = document.getElementsByClassName("haystack");

			let i = 0;
			for (let element of haystack) {
				if (firstSearchInput)
					haystackBackup.push(element.innerHTML);
				
				let text = stripTags(element.innerHTML);
				if (needle.toString() !== "") {
					let needleRegEx = new RegExp(needle, "gi");
					try {
//						Using $& within the replacement is crucial here,
//						since we want to keep the original letter casing.						
						element.innerHTML = text.replaceAll(needleRegEx, "<span class=\"search-hit\">$&</span>");
					} catch(e) {
						console.error(e);
					}
				} else {
					element.innerHTML = haystackBackup[i];
				}
				i++;
			}
			firstSearchInput = false;

		});
		
});

function sleep(timeout = 500) {
	return new Promise(resolve => setTimeout(resolve, timeout));
}

function stripTags(text) {
	return text.replaceAll(/(<([^>]+)>)/ig, "");
}

function showImage(url) {
	let container = document.createElement("div");
	let button = document.createElement("button");
	let image = document.createElement("img");
	image.className = "popup-image";
	image.src = url;
	
	container.className = "popup-image-container popup-image-container--hidden rounded shadow--custom";
	
	button.className = "btn bg-color-1 bg--gradient border--beveled shadow--custom--xs";
	button.innerHTML = "close";
	button.onclick = () => {
		document.body.removeChild(container);
	}
	
	container.appendChild(button);
	container.appendChild(image);
	document.body.appendChild(container);
	sleep(10).then( () => {
		container.classList.remove("popup-image-container--hidden");
	});
}