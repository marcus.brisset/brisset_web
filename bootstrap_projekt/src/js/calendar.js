const date = new Date();

document.addEventListener("load", createCalendar("calendarTarget", date.getFullYear(), date.getMonth()));

function createCalendar(target, setYear, setMonth) {	
	let year = setYear;
	let month = setMonth;
	let todayNumber = date.getDate();
	let todayWeekday = date.getDay() - 1 >= 0 ? date.getDay() : 6;	// we make monday 0 and sunday 6
	
//	let xhttp = new XMLHttpRequest();
//	xhttp.open("POST", "src/modules/testApi.php");
//	xhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
//	xhttp.send("calendar");
//	xhttp.onreadystatechange = function() {
//		if (this.readyState == 4 && this.status == 200) {
//			console.log(xhttp.responseText)
//		}
//	}

	const eventList = [];

	try { fetch("src/modules/calendar.php?y=" + year + "&m=" + (month + 1))
			.then( async (resolve) => {
				var eventsThisMonth = await resolve.json();
				for (event of eventsThisMonth) {
					eventList.push(event);
					
					eventItem = document.createElement("div");
					eventItem.innerHTML = event.event_title.length > 10 ? event.event_title.substr(0, 10) + "..." : event.event_title;
								
					document.getElementById("cal_day_" + event.cal_day).appendChild(eventItem);
				}
				console.log(eventList);
			});
		} catch(err) {
			console.error(err);
		}
			
	let targetElement = document.getElementById(target);
	targetElement.innerHTML = "";
	
	let totalDays = getSumDaysOfMonth(year, month);
	let firstDay = getFirstDayOfMonth(year, month);
	let sumFields = Math.ceil((firstDay + totalDays) / 7) * 7;
	let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	let weekdayNames = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];	
	
	// Build the calendar container
	let calendarContainer = document.createElement("div");
	calendarContainer.className = "calendar";
	
	let calendarHeadElement = document.createElement("div");
	calendarHeadElement.className = "calendar__head calendar--span-all";
	
	// Previous month button
	let calendarButtonPrev = document.createElement("button");
	let prevYear, prevMonth;

	if (month === 0) {
		prevYear = year - 1;
		prevMonth = 11;
	} else {
		prevYear = year;
		prevMonth = month - 1;
	}

	calendarButtonPrev.className = "btn calendar__btn";
	calendarButtonPrev.innerHTML = monthNames[prevMonth].substr(0, 3) + " " + prevYear;
	calendarButtonPrev.onclick = () => {
		createCalendar("calendarTarget", prevYear, prevMonth);
	};
	
	// Next month button
	let calendarButtonNext = document.createElement("button");
	let nextYear, nextMonth;
	if (month === 11) {
		nextYear = year + 1;
		nextMonth = 0;
	} else {
		nextYear = year;
		nextMonth = month + 1;
	}
	calendarButtonNext.className = "btn calendar__btn";
	calendarButtonNext.innerHTML = monthNames[nextMonth].substr(0, 3) + " " + nextYear;
	calendarButtonNext.onclick = () => {
		createCalendar("calendarTarget", nextYear, nextMonth);
	};
	
	// Displayed month & year
	let calendarYear = document.createElement("span");
	calendarYear.innerHTML = monthNames[month].substr(0, 3) + " " + year;
	
	calendarHeadElement.appendChild(calendarButtonPrev);
	calendarHeadElement.appendChild(calendarYear);
	calendarHeadElement.appendChild(calendarButtonNext);	
	calendarContainer.appendChild(calendarHeadElement);
	
	// Mon-Sun row
	for (i = 0; i < 7; i ++) {
		let calendarHeadElement = document.createElement("div");
		calendarHeadElement.className = "calendar__head";
		calendarHeadElement.innerHTML = weekdayNames[i].substr(0, 3);
		calendarContainer.appendChild(calendarHeadElement);
	}
	
	// Let's embrace the magic
	let dummyFields = 0, nextMonthPreviewStart = 1;
	let lastMonthLastDay = getSumDaysOfMonth(prevYear, prevMonth);
	let lastMonthPreviewStart = lastMonthLastDay - firstDay;
	
	for (i = 1; i <= sumFields; i++) {
		let calendarDayElement = document.createElement("div");
		if (i > firstDay && i - dummyFields <= totalDays) {
			let displayDay = i - dummyFields;
			 
			calendarDayElement.className = (displayDay === todayNumber && month === date.getMonth() && year === date.getFullYear()) ? "calendar__day--today" : "calendar__day";
			calendarDayElement.className += " is-link";
			calendarDayElement.id = "cal_day_" + displayDay;
			elementDayNumber = document.createElement("div");
			elementDayNumber.innerHTML = displayDay;
			calendarDayElement.appendChild(elementDayNumber);
			
			calendarDayElement.onclick = () => {
				let popoutElement = document.createElement("div");
				popoutElement.className = "calendar__day--pop-out global-invisible global-shrunk";
				let popoutHeader = document.createElement("div");
				popoutHeader.className = "pop-out__header";
				let popoutHeaderTitle = document.createElement("div");
				popoutHeaderTitle.className = "pop-out__header--title";
				
				let displayDaySuffix; // = displayDay <= 3 ? displayDay <= 2 ? displayDay <= 1 ? "st" : "nd" : "rd" : "th";
				switch(displayDay) {
					case 1:
					case 21:
					case 31:
						displayDaySuffix = "st";
						break;
					case 2:
					case 22:
						displayDaySuffix = "nd";
						break;
					case 3:
					case 23:
						displayDaySuffix = "rd";
						break;
					default:
						displayDaySuffix = "th";
				}
				
				popoutHeaderTitle.innerHTML =
						weekdayNames[new Date(year, month, displayDay - 1).getDay()]
						+ ", " + monthNames[month] + " " + displayDay + displayDaySuffix + " " + year;
				
				let popoutCloseBtn = document.createElement("button");
				popoutCloseBtn.className = "btn calendar__btn pop-out__header--btn";
				popoutCloseBtn.innerHTML = "&times;";
				popoutCloseBtn.onclick = () => popoutElement.remove();
				
				let popoutContent = document.createElement("div");
				popoutContent.id = "popOutContent";
				popoutContent.className = "pop-out__content";
				
				let noEvents = true;
				for (i in eventList) {
					if (parseInt(displayDay) === parseInt(eventList[i].cal_day)) {
						eventItem = document.createElement("div");
						eventItem.innerHTML = eventList[i].event_time + ": " + eventList[i].event_title;
						popoutContent.appendChild(eventItem);
						noEvents = false;
					}
				}
				if (noEvents) popoutContent.innerHTML = "No events.";
				
				if (month === 11 && displayDay === 24) {
					popoutContent.innerHTML += "<br /><br />Merry X-Mas!";
				}
				
				popoutHeader.appendChild(popoutHeaderTitle);
				popoutHeader.appendChild(popoutCloseBtn);
				popoutElement.appendChild(popoutHeader);
				popoutElement.appendChild(popoutContent);
				
				calendarContainer.appendChild(popoutElement);
				sleep(10).then( () => {
					popoutElement.classList.remove("global-shrunk");
					popoutElement.classList.remove("global-invisible");
				});
			};
		} else {
			dummyFields++;
			calendarDayElement.className = "calendar__blank";
			if (i <= firstDay) {
				calendarDayElement.innerHTML = lastMonthPreviewStart + 1;
				lastMonthPreviewStart++;
			} else {
				calendarDayElement.innerHTML = nextMonthPreviewStart;
				nextMonthPreviewStart++;
			}
		}
		calendarContainer.appendChild(calendarDayElement);
	}
	
	targetElement.appendChild(calendarContainer);
}

function getSumDaysOfMonth(year, month) {
	return new Date(year, month + 1, 0).getDate();	
}

function getFirstDayOfMonth(year, month) {					// returns 0-6, 0 for mon etc.
	let firstDay = new Date(year, month, 1).getDay() - 1;
	return firstDay >= 0 ? firstDay : 6;
}