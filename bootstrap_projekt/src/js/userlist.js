const perPage = 50;

document.getElementById("userListPage").addEventListener("focus", (e) => {
	e.target.select();
});

["load", "keyup", "click"].forEach(function(event) {
	window.addEventListener(event, async (e) => {
		
		if (event === "click") {
			let currentPage = document.getElementById("userListPage");

			if (e.target.id === "pageForw") {
				currentPage.value = parseInt(currentPage.value) + 1;
			}
			if (e.target.id === "pageBack") {
				currentPage.value = parseInt(currentPage.value) - 1;
			}
		}
		
		let userList = await readFile("./src/userlist.txt");
		let filteredList = new Array();
		
		document.getElementById("sumUsers").innerHTML = userList.length + " entries";

		let firstNameInput = document.getElementById("firstName");
		let lastNameInput = document.getElementById("lastName");

		let firstNameNeedle = escape(firstNameInput.value.toLowerCase());
		let lastNameNeedle = escape(lastNameInput.value.toLowerCase());

		if (firstNameNeedle.match(/\*/g) || lastNameNeedle.match(/\*/g)) {
			firstNameInput.value = firstNameNeedle = firstNameNeedle.replace(/\*/g, "");
			lastNameInput.value = lastNameNeedle = lastNameNeedle.replace(/\*/g, "");
		}

		let hitCounter = 0;
		let hitDisplay = document.getElementById("searchHits");
		if (firstNameNeedle !== "" || lastNameNeedle !== "") {
			// filter needles
			for (let user of userList) {
				let firstNameHaystack = user[0].toLowerCase();
				let lastNameHaystack = user[1].toLowerCase();
				if (firstNameHaystack.search(firstNameNeedle) !== -1 && lastNameHaystack.search(lastNameNeedle) !== -1) {
					filteredList.push(user);
					hitCounter++;
				}
			}				

			hitDisplay.innerHTML = hitCounter + " hit" + (hitCounter !== 1 ? "s" : "");
		} else {
			hitDisplay.innerHTML = "";
			filteredList = userList;
		}

		let pageInputObj = document.getElementById("userListPage");
		let pageInput = Number(pageInputObj.value);
		let lastPage = Math.ceil(filteredList.length / perPage);
		
		document.getElementById("lastPage").innerHTML = lastPage;
		
		//	checks if pageInput is neither 0 or NaN (= return values of Number() for
		//	Strings and " " (blank space) respectively, and if true checks if its
		//	above 0 and below the last possible page - assigns 1 if failing this checks
		//	let page = pageInput ? pageInput > 0 && pageInput <= lastPage ? pageInput : 1 : 1;
		//	(^^^ short version without updating the input taking page value)

		if (pageInput) {
			if (pageInput > 0 && pageInput <= lastPage) {
				page = pageInput;
			} else {
				page = pageInput > lastPage ? lastPage : 1;
				pageInputObj.value = page;
			}
		} else {
			page = 1;
			pageInputObj.value = 1;
		}

		//	toggles display of back/forward buttons depending on which page you are
		if (page > 1) {
			document.getElementById("pageBack").classList.remove("global-hidden");
		} else {
			document.getElementById("pageBack").classList.add("global-hidden");
		}
		if (page < lastPage) {
			document.getElementById("pageForw").classList.remove("global-hidden");
		} else {
			document.getElementById("pageForw").classList.add("global-hidden");
		}
		
		let start = (page - 1) * perPage;
		let pagedList = filteredList.slice(start, start + perPage);
		
		let finalList = "";
		let i = 1;
		for (let user of pagedList) {
			let firstName = user[0];
			let lastName = user[1];
			finalList += "<tr class=\"user_entry\"><td class=\"haystack\">" + (start + i) + "</td><td class=\"haystack\">" + firstName + "</td><td class=\"haystack\">" + lastName + "</td></tr>";
			i++;
		}	
		document.getElementById("userListBody").innerHTML = finalList;
	});
});

async function readFile(fileName, userList = true) {
	let stringDump = await fetch(fileName);
	let fileContent = await stringDump.text();

	if (userList) {
		let result = new Array();
		let rows = fileContent.split("\n");
		let i = 0;
		for (cell of rows) {
			result[i] = cell.split(" ");
			i++;
		}
		return result;
	} else {
		return fileContent;
	}
}  