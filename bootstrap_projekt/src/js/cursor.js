const follower = document.createElement("div");
follower.id = follower.className = "follower animation--pulse";
document.body.appendChild(follower);

["mousemove", "scroll", "mousewheel"].forEach( (event) => {
	window.addEventListener(event, (pos) => {
	
		let posXAbs = pos.clientX + window.scrollX;
		let posYAbs = pos.clientY + window.scrollY;
		
		let screenXHalf = window.screenX / 2;
		let screenYHalf = window.screenY / 2;
		
		let relativeX = pos.clientX - screenXHalf;
		let relativeY = pos.clientY - screenYHalf;

		follower.style.top = posYAbs + "px";
		follower.style.left = posXAbs + "px";
		//follower.style.transform = "skew(" + (relativeX * 0.1) + "deg, " + (relativeY * 0.1) + "deg)";
	
	});
});