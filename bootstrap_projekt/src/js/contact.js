var originalPopupText = document.getElementById("popupMessageText").innerHTML;
var originalPopupTitle = document.getElementById("popupMessageTitle").innerHTML;

document.getElementById("sendMessage").addEventListener("click", () => {
	let name = document.getElementById("messageName");
	let mail = document.getElementById("messageMail");
	let text = document.getElementById("messageText");
	
	if (name.value.trim() === "" || mail.value.trim() === "" || text.value.trim() === "") {
		document.getElementById("popupMessageTitle").innerHTML = "Ooops!"
		document.getElementById("popupMessageText").innerHTML = "Please fill out every field!";
		//document.getElementById("popupMessage").classList.remove("show");
	} else {
		document.getElementById("popupMessageTitle").innerHTML = originalPopupTitle;
		document.getElementById("popupMessageText").innerHTML = originalPopupText;
		document.getElementById("senderName").innerHTML = name.value;
		document.getElementById("senderMail").innerHTML = "(" + mail.value + ")";
		
		name.value = mail.value = text.value = "";
	}
});