document.addEventListener("load", createCalendar("calendarTarget"));

function createCalendar(target, setYear = false, setMonth = false) {
	let date = new Date();
	let year = setYear ? setYear : date.getFullYear();
	let month = setMonth ? setMonth : date.getMonth();				// getMonth() returns 0-11
	let todayNumber = date.getDate();
	let todayWeekday = date.getDay() - 1 >= 0 ? date.getDay() : 6;	// we make monday 0 and sunday 6
	
	let targetElement = document.getElementById(target);
	targetElement.innerHTML = "";
	
	let totalDays = getSumDaysOfMonth(year, month);
	let firstDay = getFirstDayOfMonth(year, month);
	let sumFields = Math.ceil((firstDay + totalDays) / 7) * 7;
	let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	let weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
	
	
	// Build the calendar container
	let calendarContainer = document.createElement("div");
	calendarContainer.className = "calendar";
	
	let calendarHeadElement = document.createElement("div");
	calendarHeadElement.className = "calendar__head calendar--span-all";
	
	// Previous month button
	let calendarButtonPrev = document.createElement("button");
	let prevYear, prevMonth;
	if (month === 0) {
		prevYear = year - 1;
		prevMonth = 11;
	} else {
		prevYear = year;
		prevMonth = month - 1;
	}
	calendarButtonPrev.className = "btn btn--calendar";
	calendarButtonPrev.innerHTML = monthNames[prevMonth] + " " + prevYear;
	calendarButtonPrev.onclick = () => {
		createCalendar("calendarTarget", prevYear, prevMonth);
		console.log(prevYear + " " + prevMonth);
	}
	
	// Next month button
	let calendarButtonNext = document.createElement("button");
	let nextYear, nextMonth;
	if (month === 11) {
		nextYear = year + 1;
		nextMonth = 0;
	} else {
		nextYear = year;
		nextMonth = month + 1;
	}
	calendarButtonNext.className = "btn btn--calendar";
	calendarButtonNext.innerHTML = monthNames[nextMonth] + " " + nextYear;
	calendarButtonNext.onclick = () => {
		createCalendar("calendarTarget", nextYear, nextMonth);
		console.log(nextYear + " " + nextMonth);
	};
	
	// Displayed month & year
	let calendarYear = document.createElement("span");
	calendarYear.innerHTML = monthNames[month] + " " + year;
	
	calendarHeadElement.appendChild(calendarButtonPrev);
	calendarHeadElement.appendChild(calendarYear);
	calendarHeadElement.appendChild(calendarButtonNext);	
	calendarContainer.appendChild(calendarHeadElement);
	
	// Mon-Sun row
	for (i = 0; i < 7; i ++) {
		let calendarHeadElement = document.createElement("div");
		calendarHeadElement.className = "calendar__head";
		calendarHeadElement.innerHTML = weekdays[i];
		calendarContainer.appendChild(calendarHeadElement);
	}
	
	// Fill in days
	let dummyFields = 0;
	for (i = 1; i <= sumFields; i++) {
		let calendarDayElement = document.createElement("div");
		if (i > firstDay && i - dummyFields <= totalDays) {
			calendarDayElement.className = (i -dummyFields === todayNumber && month === date.getMonth() && year === date.getFullYear()) ? "calendar__day--today" : "calendar__day";
			calendarDayElement.innerHTML = i - dummyFields;
		} else {
			dummyFields++;
			calendarDayElement.className = "calendar__blank";
		}
		calendarContainer.appendChild(calendarDayElement);
	}
	targetElement.appendChild(calendarContainer);
}

function getSumDaysOfMonth(year, month) {
	return new Date(year, month + 1, 0).getDate();	// +1 to get the following month, day 0 then takes the last day of the preceding month
}

function getFirstDayOfMonth(year, month) {
	let firstDay = new Date(year, month, 1).getDay() - 1;	// - 1 beacuse we want monday to be 0, not 6 (getDay() return 0-6 for sun-mon)
	return firstDay >= 0 ? firstDay : 6;					// and sunday to be 6 instead of 6
}