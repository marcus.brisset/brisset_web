
<?php

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$sql = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

if ($sql->connect_error) {
    die($sql->connect_error);
}

