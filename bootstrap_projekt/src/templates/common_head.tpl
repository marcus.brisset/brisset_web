<!DOCTYPE html>
<html lang="en">
	<head>
		<title>marcus.brisset : {$meta.page_title}</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		
		<link rel="preload" href="src/css/modes/{$color_mode.current}.css?v=1.0" as="style" />
		<link rel="preload" href="src/css/style.css?v=1.0" as="style" />
		<link id="colorMode" rel="stylesheet" href="src/css/modes/{$color_mode.current}.css?v=1.0" type="text/css" />
		<link rel="stylesheet" href="src/css/style.css?v=1.0" type="text/css" />
	</head>
	<body>
		
<!--	NAV START		-->

		<label id="pageTop"></label>
		<header class="nav-wrap">
			<nav class="navbar fixed-top navbar-dark bg-color-2 bg--gradient navbar-expand-lg shadow--custom">
				<div class="navbar__container--stretch">
					<span class="navbar-brand">
						<img id="colorModeToggler" class="navbar__icon" src="src/img/icons/{$color_mode.toggler}_mode.png" title="Switch to {$color_mode.toggler_uc} Mode" alt="{$color_mode.toggler_uc} mode" />
					</span>
					<span class="navbar-brand navbar__search-wrap">
						<input type="text" class="navbar__search-bar form-control" id="navSearchBar" placeholder="search..." />
					</span>
				</div>
				
				<button class="btn btn-dark navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbar-content"><span class="navbar-toggler-icon"></span></button>
				
				<div class="collapse navbar-collapse" id="navbar-content">
					<ul class="navbar-nav mr-auto">
{foreach from=$nav_items item=$item}
						<li class="nav-item">
							<a class="nav-link{if $item->getIsActive()} nav-link--active{/if}" href="{$item->getUrl()}">{$item->getText()}</a>
						</li>
{/foreach}
					</ul>
				</div>
			</nav>
		</header>
