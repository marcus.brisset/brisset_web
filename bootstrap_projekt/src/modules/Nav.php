<?php

/*
 *	Had to remove all type declarations, apparently that's a PHP 8 feature.
 *	(private array $navItems > private $navItems ... and so on)
 */

class Nav {
	
	private $navItems;
	private $activeUrl;
	
	public function __construct($url = "") {
		$this->setActiveUrl($url);
	}
	
	public function addItem(string $url, string $text): void {
		$flagIsActivePage = false;
		if (preg_match("/" . $url . "/i", $this->activeUrl)) {
			$flagIsActivePage = true;
		}
		$this->navItems[] = new Item($url, $text, $flagIsActivePage);
	}
	
	public function setActiveUrl(string $url): void {
		$this->activeUrl = $url;
	}
	
	public function getItems(): array {
		return $this->navItems;
	}

}

class Item extends Nav {
	
	private $url, $text;
	private $isActive;
	
	public function __construct(string $url, string $text, bool $isActive) {
		$this->url = $url;
		$this->text = $text;
		$this->isActive = $isActive;
	}
	
	public function getUrl(): string {
		return $this->url;
	}

	public function getText(): string {
		return $this->text;
	}
	
	public function getIsActive(): bool {
		return $this->isActive;
	}

}