<?php
require("../constants.php");
require("../mysql_init.php");

if (isset($_GET["y"]) && isset($_GET["m"])) {
	$year = $_GET["y"];
	$month = $_GET["m"];
	$query = "
		SELECT * FROM events
		WHERE
			events.event_time <= " . mktime(0, 0, 0, $month, cal_days_in_month(CAL_GREGORIAN, $month, $year), $year) . " 
			AND events.event_time >= " . mktime(0, 0, 0, $month, 1, $year) . "
		";
	
	$result = $sql->query($query);
	$events = array();
	$i = 0;
	while ($row = $result->fetch_assoc()) {
		$events[$i] = $row;
		$events[$i]["cal_day"] = date("j", $row["event_time"]);
		$events[$i]["event_time"] = date("H:i", $row["event_time"]);
		$i++;
	}
	
	echo json_encode($events);
}
