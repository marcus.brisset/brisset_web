<?php
/* Smarty version 3.1.39, created on 2021-11-28 12:47:01
  from 'C:\xampp\htdocs\brisset_web\bootstrap_projekt\src\templates\common_head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a36c35674e77_82119836',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7e0aace47dbe51244b61b90b1d2c38b52f90f80e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\brisset_web\\bootstrap_projekt\\src\\templates\\common_head.tpl',
      1 => 1638100019,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61a36c35674e77_82119836 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
	<head>
		<title>marcus.brisset : <?php echo $_smarty_tpl->tpl_vars['meta']->value['page_title'];?>
</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		
		<link id="colorMode" rel="stylesheet" href="src/css/modes/<?php echo $_smarty_tpl->tpl_vars['color_mode']->value['current'];?>
.css?v=1.0" type="text/css" />
		<link rel="stylesheet" href="src/css/style.css?v=1.0" type="text/css" />
	</head>
	<body>
		
<!--	NAV START		-->

		<div class="nav-wrap">
			<nav class="navbar fixed-top navbar-dark bg-color-2 bg--gradient navbar-expand-lg shadow--custom">
				<div class="navbar__container--stretch">
					<span class="navbar-brand">
						<img id="colorModeToggler" class="navbar__icon" src="src/icons/<?php echo $_smarty_tpl->tpl_vars['color_mode']->value['toggler'];?>
_mode.png" title="Switch to Light Mode" alt="Light mode" />
					</span>
					<span class="navbar-brand navbar__search-wrap">
						<input type="text" class="navbar__search-bar form-control" id="navSearchBar" placeholder="search..." />
					</span>
				</div>
				
				<button class="btn btn-dark navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbar-content"><span class="navbar-toggler-icon"></span></button>
				
				<div class="collapse navbar-collapse" id="navbar-content">
					<ul class="navbar-nav mr-auto">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['nav_items']->value, 'item');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
						<li class="nav-item">
							<a class="nav-link<?php if ($_smarty_tpl->tpl_vars['item']->value->getIsActive()) {?> nav-link--active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['item']->value->getUrl();?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->getText();?>
</a>
						</li>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</ul>
				</div>
			</nav>
		</div>
<?php }
}
