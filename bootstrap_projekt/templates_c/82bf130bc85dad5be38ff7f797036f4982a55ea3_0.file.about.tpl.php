<?php
/* Smarty version 3.1.39, created on 2021-11-28 11:54:19
  from 'C:\xampp\htdocs\brisset_web\bootstrap_projekt\src\templates\about.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a35fdb36bb58_00964216',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '82bf130bc85dad5be38ff7f797036f4982a55ea3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\brisset_web\\bootstrap_projekt\\src\\templates\\about.tpl',
      1 => 1638096810,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61a35fdb36bb58_00964216 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!--	TEMPLATE START		-->

		<main>
			
			<div class="d-flex flex-wrap gap-3">
				
				<div class="card order-0 order-lg-2 flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient">photo</div>
					<div class="card-body">
						<img class="img--w-20 rounded mx-auto d-block border border--inverted" src="src/img/brisset.jpg" alt="Picture of Marcus Brisset" title="Picture of Marcus Brisset" />
					</div>
				</div>
				
				<div class="card order-1 flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient">personal</div>
					<div class="card-body">
						<h5 class="card-title haystack">Marcus-Christian Brisset</h5>
						<div class="row m-1">
							<div class="col-4 p-1 fw-bold haystack">Name:</div>
							<div class="col p-1 haystack">Marcus-Christian Brisset</div>				
						</div>
						<div class="row m-1">
							<div class="col-4 p-1 fw-bold haystack">Date of birth:</div>
							<div class="col p-1 haystack">12.04.1984</div>				
						</div>
						<div class="row m-1">
							<div class="col-4 p-1 fw-bold haystack">Address:</div>
							<div class="col p-1 haystack">Johnstraße 83/7 A-1150 Wien</div>				
						</div>
						<div class="row m-1">
							<div class="col-4 p-1 fw-bold haystack">Mobile:</div>
							<div class="col p-1 haystack">+43 (680) 22 100 71</div>				
						</div>
						<div class="row m-1">
							<div class="col-4 p-1 fw-bold haystack">Mail:</div>
							<div class="col p-1 haystack">marcus.c.brisset@gmail.com</div>				
						</div>
						<a href="src/pdf/brisset_cv_final.pdf" class="btn bg-color-1 bg--gradient border mt-2 shadow--custom--xs">Download my CV</a>
					</div>
				</div>

<!--	CAREER START	-->

				<div class="card order-2 w-100 bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient">career</div>
					<div class="card-body">
						<h6 class="card-title h--pre">12/2020 - ongoing</h6>
						<h5 class="card-title h--has-border haystack">Skill enhancement and job-seeking</h5>
						<div class="row p-3">
							<div class="col-md">
								
								<h3 class="text-color-1"><span class="highlight">&raquo;</span> <span class="haystack">CODERS.BAY Vienna</span> <span class="h--sub">(08/2021 - 03/22)</span></h3>
								<ul>
									<li class="haystack">Software Development Course</li>
									<li class="haystack">Qualifying passed (5 weeks of Web, Java, Career coaching, Project management, Network technology)</li>
									<li class="haystack">
										<b>Key aspects:</b>
										<br />Java, Spring Boot, Spring Security, Maven & Gradle, JUnit, Android Studio
										<br />HTML, CSS, Bootstrap, Javascript, TypeScript, Vue
										<br />MySQL, NoSQL
										Git, Agiles Projektmanagement (Scrum)										
									</li>
								</ul>
								
								<h3 class="text-color-1"><span class="highlight">&raquo;</span> <span class="haystack">die Berater</span> <span class="h--sub">(03/21 – 05/21)</span></h3>
								<ul>
									<li class="haystack">Language course for Business English B2 </li>
									<li class="haystack">Graduated <span class="highlight">C</span></li>
								</ul>
								
							</div>
						</div>
						
					</div>
				</div>

				<div class="card order-2 w-100 bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient">career</div>
					<div class="card-body">
						<h6 class="card-title h--pre">01/2016 - 11/2020</h6>
						<h5 class="card-title h--has-border haystack">Logistics Operative</h5>
						<div class="row p-3">
							<div class="col-md">
								
								<h3 class="text-color-1"><span class="highlight">&raquo;</span> <span class="haystack">Otto Bock Healthcare Products GmBH</span></h3>
								<ul>
									<li class="haystack">Packaging medical devices in compliance with the corresponding regulations and specifications</li>
									<li class="haystack">ESD Training</li>
								</ul>
								
							</div>
						</div>
						
					</div>
				</div>
				
				<div class="card order-2 w-100 bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient">career</div>
					<div class="card-body">
						<h6 class="card-title h--pre">06/2014 - 09/2015</h6>
						<h5 class="card-title h--has-border haystack">Logistics Operative</h5>
						<div class="row p-3">
							<div class="col-md">
								
								<h3 class="text-color-1"><span class="highlight">&raquo;</span> <span class="haystack">Logistik Park 19 GmbH</span></h3>
								<ul>
									<li class="haystack">Packaging and shipping automobile parts</li>
									<li class="haystack">Stock receipt</li>
									<li class="haystack">Forklift Operator</li>
								</ul>
								
							</div>
						</div>
						
					</div>
				</div>


			</div>
			
		</main>

<!--	TEMPLATE END		-->
<?php }
}
