<?php
/* Smarty version 3.1.39, created on 2021-11-28 11:56:12
  from 'C:\xampp\htdocs\brisset_web\bootstrap_projekt\src\templates\disclaimer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a3604c687728_32814379',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c4c8f07ee4c7342c988d617fb3cbb8e46cecfc07' => 
    array (
      0 => 'C:\\xampp\\htdocs\\brisset_web\\bootstrap_projekt\\src\\templates\\disclaimer.tpl',
      1 => 1638096810,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61a3604c687728_32814379 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!--	TEMPLATE START		-->

		<main>
			
			<div class="rounded-3 p-4 bg-color-1 border shadow--custom--s">
				
				<h1 class="h--has-border haystack">Disclaimer for mbrisset.bplaced.net</h1>
				<p class="haystack">If you require any more information or have any questions about our site's disclaimer, please feel free to contact us by email at marcus.c.brisset@gmail.com. Our Disclaimer was generated with the help of the <a href="https://www.disclaimergenerator.net/">Disclaimer Generator</a>.</p>

				<h2 class="h--has-border haystack">Disclaimers for mbrisset.bplaced.net</h2>
				<p class="haystack">All the information on this website - http://mbrisset.bplaced.net - is published in good faith and for general information purpose only. mbrisset.bplaced.net does not make any warranties about the completeness, reliability and accuracy of this information. Any action you take upon the information you find on this website (mbrisset.bplaced.net), is strictly at your own risk. mbrisset.bplaced.net will not be liable for any losses and/or damages in connection with the use of our website.</p>
				<p class="haystack">From our website, you can visit other websites by following hyperlinks to such external sites. While we strive to provide only quality links to useful and ethical websites, we have no control over the content and nature of these sites. These links to other websites do not imply a recommendation for all the content found on these sites. Site owners and content may change without notice and may occur before we have the opportunity to remove a link which may have gone 'bad'.</p>
				<p class="haystack">Please be also aware that when you leave our website, other sites may have different privacy policies and terms which are beyond our control. Please be sure to check the Privacy Policies of these sites as well as their "Terms of Service" before engaging in any business or uploading any information.</p>

				<h2 class="h--has-border haystack">Consent</h2>
				<p class="haystack">By using our website, you hereby consent to our disclaimer and agree to its terms.</p>

				<h2 class="h--has-border haystack">Update</h2>
				<p class="haystack">Should we update, amend or make any changes to this document, those changes will be prominently posted here.</p>

				<h2 class="h--has-border haystack">Sources</h2>
				<p class="haystack">(2021) Images from <a href="https://unsplash.com/" title="https://unsplash.com/">https://unsplash.com/</a></p>
				
			</div>
					
		</main>		

<!--	TEMPLATE END		-->
<?php }
}
