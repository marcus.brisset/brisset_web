<?php
/* Smarty version 3.1.39, created on 2021-11-28 11:52:43
  from 'C:\xampp\htdocs\brisset_web\bootstrap_projekt\src\templates\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a35f7b749416_10830875',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5ed8caa5fdfa5544e2f914887832239bd85ec59f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\brisset_web\\bootstrap_projekt\\src\\templates\\index.tpl',
      1 => 1638096759,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61a35f7b749416_10830875 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!--	TEMPLATE START		-->

		<main>
			
<!--	CARDS START		-->
			
			<div class="card-box d-flex flex-wrap gap-3">
				
				<div class="card flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient haystack">update</div>
					<div class="card-body">
						<h5 class="card-title haystack">Images</h5>
						<p class="card__text haystack">
							<img class="img--intext img--intext-left is-link" onclick="showImage(this.src);" src="src/img/lara_01.jpg" alt="Bild von Lara" title="Bild von Lara" />
							Images which provide an onclick-event (and therefore show a pointer cursor when hovering over them) now present themselves in all of their glory when clicked.
						</p>
					</div>
				</div>
				
				<div class="card flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient haystack">update</div>
					<div class="card-body">
						<h5 class="card-title haystack">Dark / Light Mode</h5>
						<p class="card__text haystack">
							You can now switch between dark and light mode. If you choose to navigate the php version of this site you won't have to deal with the on-load glitch when using light mode.
						</p>
					</div>
				</div>
				
				<div class="card flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient haystack">update</div>
					<div class="card-body">
						<h5 class="card-title haystack">Search function</h5>
						<p class="card__text haystack">
							The original page content will now be restored again once the search input field is being left empty.
						</p>
					</div>
				</div>
				
				<div class="card flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient haystack">flashnews</div>
					<div class="card-body">
						<h5 class="card-title haystack">CV online</h5>
						<p class="card__text haystack">
							From now on you can view and download my CV.
							NOT... Just needed to fill this card with ANYthing.
						</p>
						<p class="card__text haystack">
							UPDATE: CV now available, plus it's an updated version.
						</p>
						<a href="about.php" class="btn bg-color-1 bg--gradient border shadow--custom--xs">To the CV</a>
					</div>
				</div>
				
				<div class="card flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient haystack">coding</div>
					<div class="card-body">
						<h5 class="card-title haystack">Javascript demo</h5>
						<p class="card__text haystack">
							Since we aren't using any kind of data base yet, I used a .txt file to fetch
							a list of randomly generated names from. Results are displayed in sets of 50 per page.
							<br />
							<br />You can search by both first and last name.
						</p>
						<a href="javascript.php" class="btn bg-color-1 bg--gradient border shadow--custom--xs">Javascript</a>
					</div>
				</div>
				
				<div class="card flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient haystack">coding</div>
					<div class="card-body">
						<h5 class="card-title haystack">Search</h5>
						<p class="card__text haystack">
							Did you know, you can search on every page via the search bar at the top of the screen?
							<br />It will highlight all matching patterns within elements classified as "haystack".
							<br />Unfortunately, since the script strips haystacks of all its HTML-elements before
							displaying matches, ...see for yourself :D
						</p>
						<a href="#" class="btn bg-color-1 bg--gradient border shadow--custom--xs" onclick="document.getElementById('navSearchBar').focus();">Try it out</a>
					</div>
				</div>
				
				<div class="card flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient haystack">update</div>
					<div class="card-body">
						<h5 class="card-title haystack">Not so much</h5>
						<p class="card__text haystack">
							Still trying to figure out what to do with this project, still not having a freakin clue.
						</p>
						<a href="#" class="btn bg-color-1 bg--gradient border shadow--custom--xs">Nothing to see here</a>
					</div>
				</div>
				
				<div class="card flex-fill bg-color-1 border shadow--custom--s">
					<div class="card-header bg--gradient haystack">flashnews</div>
					<div class="card-body">
						<h5 class="card-title haystack">The first stone has been thrown!</h5>
						<p class="card__text haystack">
							Here we go.
						</p>
						<a href="#" class="btn bg-color-1 bg--gradient border shadow--custom--xs">Button ain't working</a>
					</div>
				</div>
				
			</div>

<!--	TEXT START		-->
			
			<div class="mt-3 border rounded-3 p-4 bg-color-1 shadow--custom--s">
				
				<h2 class="haystack h--has-border">Lorem Ipsum</h2>
				<p class="haystack">
					Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   
					Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   
					Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   
				</p>
				
				<p class="haystack">
					<img class="img--intext img--intext-left is-link" onclick="showImage(this.src);" src="src/img/lara_01.jpg" alt="Bild von Lara" title="Bild von Lara" />
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis nunc sed dolor vehicula commodo. Morbi porttitor dictum ultrices. Vivamus ac neque quis arcu commodo finibus ut a nibh. Donec mollis magna mattis leo tristique, in lacinia nibh finibus. Praesent in velit dapibus risus molestie porta. Maecenas id nisi vel nisl dignissim vehicula. Nunc ullamcorper pulvinar elit nec sodales. Ut ante enim, sodales vel porta non, varius at nulla.
					In non nibh consequat, congue mauris ut, bibendum turpis. Duis iaculis nunc et libero ultricies, at eleifend justo aliquam. Maecenas pulvinar bibendum aliquam. In felis eros, tempor ac convallis ac, ornare quis eros. Maecenas scelerisque lobortis dui, ac faucibus ex accumsan ac. Curabitur vel ex nec mi cursus viverra eget eu dolor. Integer porttitor neque id ullamcorper porttitor. Sed nec diam non nulla pretium venenatis mollis eget eros. Vivamus nec erat faucibus, sagittis sem sed, iaculis turpis.
					Nam nisi dolor, aliquet a sapien tincidunt, egestas tincidunt leo. Maecenas viverra convallis leo. Vivamus scelerisque eros ac ultrices efficitur. Nunc fringilla gravida sodales. Cras eu sem ante. Sed eu porta neque. Etiam eu justo ligula. Integer laoreet sem in ante dapibus blandit non eget orci. Ut at cursus velit, non congue est. Donec porta ultricies elit, at volutpat metus fermentum in. Nulla eu sapien tincidunt, sagittis nulla sit amet, vehicula nunc. Sed tempus sem a odio placerat varius. Suspendisse ut elit sed mi vulputate tempus.
					Aliquam erat volutpat. Mauris ultrices, lorem nec bibendum dignissim, magna nibh pulvinar libero, ut lacinia elit diam a sapien. Praesent molestie ex mauris. Etiam quis sollicitudin mi. Duis accumsan suscipit consectetur. Integer velit sapien, sollicitudin sit amet metus et, venenatis sodales arcu. Fusce a libero eu sem placerat tincidunt. Proin tincidunt magna lorem, vel imperdiet felis porttitor a.
					<img class="img--intext img--intext-right is-link" onclick="showImage(this.src);" src="src/img/lara_02.jpg" alt="Bild von Lara" title="Bild von Lara" />
					Fusce at vestibulum nibh, sed pharetra elit. Nulla placerat, massa a sagittis condimentum, purus risus molestie nisi, vitae interdum eros nibh nec ligula. Aenean fermentum eros id pretium viverra. Sed in arcu eget sem accumsan euismod. Praesent odio est, vulputate in arcu vel, faucibus cursus velit. Sed molestie aliquam tortor non dapibus. Curabitur eros lorem, hendrerit ut tristique vel, malesuada eget libero.
					<img class="img--intext img--intext-left is-link" onclick="showImage(this.src);" src="src/img/lara_03.jpg" alt="Bild von Lara" title="Bild von Lara" />
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis nunc sed dolor vehicula commodo. Morbi porttitor dictum ultrices. Vivamus ac neque quis arcu commodo finibus ut a nibh. Donec mollis magna mattis leo tristique, in lacinia nibh finibus. Praesent in velit dapibus risus molestie porta. Maecenas id nisi vel nisl dignissim vehicula. Nunc ullamcorper pulvinar elit nec sodales. Ut ante enim, sodales vel porta non, varius at nulla.
					In non nibh consequat, congue mauris ut, bibendum turpis. Duis iaculis nunc et libero ultricies, at eleifend justo aliquam. Maecenas pulvinar bibendum aliquam. In felis eros, tempor ac convallis ac, ornare quis eros. Maecenas scelerisque lobortis dui, ac faucibus ex accumsan ac. Curabitur vel ex nec mi cursus viverra eget eu dolor. Integer porttitor neque id ullamcorper porttitor. Sed nec diam non nulla pretium venenatis mollis eget eros. Vivamus nec erat faucibus, sagittis sem sed, iaculis turpis.
					Nam nisi dolor, aliquet a sapien tincidunt, egestas tincidunt leo. Maecenas viverra convallis leo. Vivamus scelerisque eros ac ultrices efficitur. Nunc fringilla gravida sodales. Cras eu sem ante. Sed eu porta neque. Etiam eu justo ligula. Integer laoreet sem in ante dapibus blandit non eget orci. Ut at cursus velit, non congue est. Donec porta ultricies elit, at volutpat metus fermentum in. Nulla eu sapien tincidunt, sagittis nulla sit amet, vehicula nunc. Sed tempus sem a odio placerat varius. Suspendisse ut elit sed mi vulputate tempus.
					Aliquam erat volutpat. Mauris ultrices, lorem nec bibendum dignissim, magna nibh pulvinar libero, ut lacinia elit diam a sapien. Praesent molestie ex mauris. Etiam quis sollicitudin mi. Duis accumsan suscipit consectetur. Integer velit sapien, sollicitudin sit amet metus et, venenatis sodales arcu. Fusce a libero eu sem placerat tincidunt. Proin tincidunt magna lorem, vel imperdiet felis porttitor a.
					<img class="img--intext img--intext-right is-link" onclick="showImage(this.src);" src="src/img/lara_04.jpg" alt="Bild von Lara" title="Bild von Lara" />
					Fusce at vestibulum nibh, sed pharetra elit. Nulla placerat, massa a sagittis condimentum, purus risus molestie nisi, vitae interdum eros nibh nec ligula. Aenean fermentum eros id pretium viverra. Sed in arcu eget sem accumsan euismod. Praesent odio est, vulputate in arcu vel, faucibus cursus velit. Sed molestie aliquam tortor non dapibus. Curabitur eros lorem, hendrerit ut tristique vel, malesuada eget libero.
				</p>
				
			</div>
			
		</main>		

<!--	TEMPLATE END		-->
<?php }
}
