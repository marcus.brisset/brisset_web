<?php
/* Smarty version 3.1.39, created on 2021-11-28 11:55:56
  from 'C:\xampp\htdocs\brisset_web\bootstrap_projekt\src\templates\javascript.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a3603cad1e57_05898329',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9e81dc10e44e59cfaf64b32fac590e7e16b68027' => 
    array (
      0 => 'C:\\xampp\\htdocs\\brisset_web\\bootstrap_projekt\\src\\templates\\javascript.tpl',
      1 => 1638096810,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61a3603cad1e57_05898329 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!--	TEMPLATE START		-->

		<main>
			
<!--	SEARCH START	-->
			
			<div class="border rounded-3 p-4 bg-color-2 bg--gradient shadow--custom--s mb-2">
				<h2 class="form__header h--has-border">Search</h2>
				<div class="row m-1">
					<div class="col-sm col--mw-10">First Name:</div>
					<div class="col-sm"><input class="form-control form-control-sm" type="text" name="first_name" id="firstName" /></div>
				</div>
				<div class="row m-1">				
					<div class="col-sm col--mw-10">Last Name:</div>
					<div class="col-sm"><input class="form-control form-control-sm" type="text" name="last_name" id="lastName" /></div>
				</div>
				<div>
					<div class="text-center" id="sumUsers"></div>
					<div class="text-center font-weight-bold smaller highlight" id="searchHits"></div>
				</div>
			</div>

<!--	PAGER START		-->

			<div class="pager-container row m-1">
				<div class="container d-flex justify-content-center">
					<div class="pager">
						<button id="pageBack" class="btn py-1 px-2 bg-color-1 bg--gradient border shadow--custom--s global-hidden">BACK</button>
						<div class="d-inline-block p-1 bg-color-1 bg--gradient rounded border my-auto shadow--custom--s">
							<input id="userListPage" class="form-control input-xs text-center bg-color-3 border border--inverted bg--gradient" type="text" name="user_list_page" value="1" />
							of <span id="lastPage"></span></div>
						<button id="pageForw" class="btn py-1 px-2 bg-color-1 bg--gradient border shadow--custom--s global-hidden">NEXT</button>
					</div>
				</div>
			</div>

<!--	TEXTFILE START	-->

			<table class="table table-hover mt-2 bg-color-2 border shadow--custom--s">
				<thead>
					<tr>
						<th class="bg-color-0 bg--gradient">#</th>
						<th class="bg-color-0 bg--gradient">First name</th>
						<th class="bg-color-0 bg--gradient">Last name</th>
					</tr>
				</thead>
				<tbody id="userListBody">
				</tbody>				
			</table>
			
		</main>
		<?php echo '<script'; ?>
 src="src/js/userlist.js"><?php echo '</script'; ?>
>

<!--	TEMPLATE END		-->
<?php }
}
