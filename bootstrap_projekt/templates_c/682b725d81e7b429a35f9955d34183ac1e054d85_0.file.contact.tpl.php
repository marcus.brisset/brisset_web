<?php
/* Smarty version 3.1.39, created on 2021-11-28 11:53:44
  from 'C:\xampp\htdocs\brisset_web\bootstrap_projekt\src\templates\contact.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a35fb81800d0_97225445',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '682b725d81e7b429a35f9955d34183ac1e054d85' => 
    array (
      0 => 'C:\\xampp\\htdocs\\brisset_web\\bootstrap_projekt\\src\\templates\\contact.tpl',
      1 => 1638096810,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61a35fb81800d0_97225445 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!--	TEMPLATE START		-->

		<main>
			
			<div class="border rounded-3 p-4 bg-color-2 bg--gradient shadow--custom--s">
				<h2 class="form__header h--has-border">Get in touch</h2>
				
				<!--
				Removed the "required" attriubte from all input fields and stopped the
				form from being submitted - for demo purposes
				Haven't had the time to dig deep enough into bootstrap, so I just altered
				the modals' content depending on whether or not the form is filled out properly.
				-->
				<form onsubmit="return false;">
					<div class="row m-1">
						<div class="col-md col--mw-10">Name:</div>
						<div class="col-md">
							<input class="form-control form-control-sm" type="text" name="message_name" id="messageName" />
						</div>
					</div>
					<div class="row m-1">
						<div class="col-md col--mw-10">E-mail:</div>
						<div class="col-md">
							<input class="form-control form-control-sm" type="text" name="message_mail" id="messageMail" />
						</div>
					</div>
					<div class="row m-1">
						<div class="col-md col--mw-10">Message:</div>
						<div class="col-md">
							<textarea class="form-control" name="message_text" id="messageText" rows="3"></textarea>
						</div>
					</div>
					<div class="row m-1 mt-3">
						<div class="col-md text-center">
							<input class="btn bg-color-1 bg--gradient border shadow--custom--xs" data-bs-toggle="modal" data-bs-target="#popupMessage" type="submit" name="send_message" id="sendMessage" value="Send" />
						</div>
					</div>
				</form>
			</div>
			
<!--	POPUP START		-->
			
			<div class="modal fade conditional" id="popupMessage" tabindex="-1" aria-labelledby="popupMessage" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content bg-color-1 border">
						<div class="modal-header bg-color-1 bg--gradient">
							<h5 id="popupMessageTitle" class="modal-title">Submitted!</h5>
							<button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div id="popupMessageText" class="modal-body">
							Thank you <span class="fw-bold highlight" id="senderName"></span> for contacting me,
							i will get back to you <span class="fw-bold highlight" id="senderMail"></span> as soon as possible.
						</div>
						<div class="modal-footer">
							<button type="button" class="btn bg-color-1 bg--gradient border" data-bs-dismiss="modal">Okay</button>
						</div>
					</div>
				</div>
			</div>
		
		</main>		
		<?php echo '<script'; ?>
 src="src/js/contact.js"><?php echo '</script'; ?>
>

<!--	TEMPLATE END		-->
<?php }
}
