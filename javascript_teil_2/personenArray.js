const thisYear = new Date().getFullYear();

class Person {
	
	constructor(firstName, lastName, birthYear) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthYear = birthYear;
	}
	
	get getAge() {
		return thisYear - this.birthYear;
	}
	
	get getFirstName() {
		return this.firstName;
	}
	
	set setFirstName(newFirstName) {
		this.firstName = newFirstName;
	}
	
	get getLastName() {
		return this.lastName;
	}
	
	set setLastName(newLastName) {
		this.lastName = newLastName;
	}
	
	get getBirthYear() {
		return this.birthYear;
	}
	
	set setBirthYear(newBirthYear) {
		this.birthYear = newBirthYear;
	}
	
}

var personen = [];

function updateTable() {
	let tableBody = document.getElementById("listBody");
	let tableDump = "";
	
	for (person of personen) {
		tableDump += "<tr><td>" + person.firstName + "</td>";
		tableDump += "<td>" + person.lastName + "</td>";
		tableDump += "<td>" + person.getAge + "</td></tr>";
	}
	
	tableBody.innerHTML = tableDump;
}

function addPerson() {
	let firstName = document.getElementById("firstName");
	let lastName = document.getElementById("lastName");
	let birthYear = document.getElementById("birthYear");
	
	if ((firstName.value.trim() && lastName.value.trim() && birthYear.value.trim()) !== "") {	
		personen.push(new Person(firstName.value, lastName.value, birthYear.value));
		firstName.value = lastName.value = birthYear.value = "";
	
		updateTable();
	} else {
		alert("Eingaben unvollstaendig")
	}
}

function removeLast() {
	personen.pop();
	updateTable();
}

function removeAll() {
	personen = [];
	updateTable();
}

function sortBy(column) {
	switch (column) {
		case "lastName":
			personen.sort( (a, b) => {
				let lastNameA = a.lastName;
				let lastNameB = b.lastName;

				if (lastNameA < lastNameB)
					return -1;
				if (lastNameA > lastNameB)
					return 1;
				
				return 0;
			});
			break;
			
		case "age":
			personen.sort( (a, b) => a.getAge - b.getAge);
			break;
	}
	updateTable();
}

// Habe die sort Funktionen zusammengefügt
// 
//function sortByLastName() {
//	personen.sort( (a, b) => {
//		let lastNameA = a.lastName;
//		let lastNameB = b.lastName;
//
//		if (lastNameA < lastNameB)
//			return -1;
//		if (lastNameA > lastNameB)
//			return 1;
//		
//		return 0;
//	});
//	updateTable();
//}
//
//function sortByAge() {
//	personen.sort( (a, b) => a.getAge - b.getAge);
//	updateTable();
//}