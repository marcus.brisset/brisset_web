var money = 100;

var wallet = document.getElementById("money");
var startBtn = document.getElementById("startBtn");
var spinResult = document.getElementById("result");
wallet.innerHTML = money;

var betHistory = [];
var resultCounter = {black: 0, red: 0, green: 0};

function gamble() {
	let wager = document.getElementById("wager");
	let bet = parseInt(wager.value);
	let choice = document.getElementById("colorRed").checked ? "red" : "black";
	
	// parseInt() returns NaN on strings, therefore
	if (isNaN(bet)) bet = 0;
	
	if (bet < 0) {
		msg("Ihrem Einsatz wurde das Vorzeichen entfernt!");
		wager.value = bet = Math.abs(bet);
		
	} else if (bet > 0) {
		
		if (bet <= money) {
			spinResult.classList.add("spinning");
			
			// disabling startBtn while spin gif is up, finally() enables it again
			startBtn.disabled = true;
			sleep(2000).then( () => {
				spinResult.classList.remove("spinning");
				let result = spin();

				if (choice === result) {
					money += bet;
					swapClasses(spinResult, "won", "lost");
					msg("Gewonnen!", "result");
				} else {
					money -= bet;
					swapClasses(spinResult, "lost", "won");
					msg("Verloren...", "result");
				}
				
				betHistory.unshift(new BetHistory(bet, choice, result));
				printBetHistory();
				wallet.innerHTML = money;
				
				document.getElementById("resultCounter").innerHTML = 
						" Runs: " + parseInt(resultCounter.black + resultCounter.red + resultCounter.green)
						+ " (<span class=\"text-red\">R: " + resultCounter.red + "</span>"
						+ " | B: " + resultCounter.black
						+ " | <span class=\"text-green\">G: " + resultCounter.green + "</span>)";
				
			}).finally( () => startBtn.disabled = false );
		} else {
			msg("Sie haben nicht genügend Geld!");
		}
	}else {
		msg("Sie haben nichts gesetzt!");		
	}
}

function printBetHistory() {
	betHistoryTable = document.getElementById("betHistoryTable");
	tableDump = "";
	
	for (let bet of betHistory) {
		// color defining classes depend on bet outcome
		betWinLoss = bet.choice === bet.result ? "win" : "loss";
		tableDump += "<tr class=\"history-" + betWinLoss + "\"><td class=\"text-right\">" + bet.amount + "</td>";
		tableDump += "<td class=\"text-center font-weight-bold text-" + bet.choice + "\">" + bet.choice.toUpperCase() + "</td>";
		tableDump += "<td class=\"text-center font-weight-bold text-" + bet.result + "\">" + bet.result.toUpperCase() + "</td></tr>";
	}
	
	betHistoryTable.innerHTML = tableDump;
}

function swapClasses(object, addClass, removeClass) {
	object.classList.add(addClass);
	object.classList.remove(removeClass);
}


function spin() {
	let number = randBetween(1, 100);
	let color = "green";
	
	if (number <= 48) {
		color = "red";
		resultCounter.red++;
	} else if (number >= 53) {
		color = "black";
		resultCounter.black++;
	} else {
		resultCounter.green++;
	}
	
	// value of 'color' will always be a viable css color string, so:
	spinResult.style.backgroundColor = color;
	return color;
}

function msg(text, target = "error") {
	targetElement = document.getElementById(target);
	targetElement.innerHTML = text;
	sleep().then( () => {
		targetElement.innerHTML = "";
	});
}

function sleep(duration = 1500) {
	return new Promise(resolve => setTimeout(resolve, duration));
}

function randBetween(min, max) {
	return Math.floor((Math.random() * (max - min + 1) + min));
}

class BetHistory {
	
	constructor(amount = 0, choice = "", result = "") {
		this.amount = amount;
		this.choice = choice;
		this.result = result;
	}
	
}